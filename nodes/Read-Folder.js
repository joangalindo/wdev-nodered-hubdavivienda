var app = require('../core/app')
module.exports = function (RED) {
    function ListFolderArticle(config) {
        var core = new app();
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function (msg) {
            var event = {
                payload: msg.payload,
                config: config
            };
            core.listFolder(event, (res)=>{
                if(res.error){
                    msg.statusCode = 400;
                }else{
                    msg.statusCode = 200;
                }
                msg.payload = res;
                node.send(msg);
            });
        });
    }
    RED.nodes.registerType("Read-folder", ListFolderArticle);
}