var app = require('../core/app')
module.exports = function (RED) {
    function DeleteFolder(config) {
        var core = new app();
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function (msg) {
            var event = {
                payload: msg.payload,
                config: config,
                queryParam: msg.req.query
            };
            core.deleteFolder(event, (res)=>{
                if(res.error){
                    msg.statusCode = 400;
                }else{
                    msg.statusCode = 200;
                }
                msg.payload = res;
                node.send(msg);
            });
        });
    }
    RED.nodes.registerType("Delete-folder", DeleteFolder);
}