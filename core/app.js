var wlog = require('wsuite-logger');
var mysqlUtility = require('./lib/mysql-utility')
var s3Utility = require('./lib/s3-utility')
var async = require('async');
let v = require("./lib/nestedvalidation");
var gm = require('gm').subClass({
    imageMagick: true
});

wlog.init();

module.exports = class execute {

    constructor() {
        wlog.debug('Constructor');
    }

    /**
     * folder listing with pagination
     * @param event get api request data
     * @param callback
     */
    listFolder(event, callback) {
        var response = {
            error: false,
            message: "",
            data: null
        };
        let validationSchema = {};
        let result = v.validateform(Object.assign(event.config, event.payload), validationSchema);
        if (result.error) {
            response.error = true;
            response.message = "Invalid Request Parameters or configuration missing";
            callback(response)
        } else {
            let params = event.payload;
            mysqlUtility.getFolder(event.config, params, (res) => {
                if (res.error) {
                    response.error = true;
                    response.message = res.msg;
                    callback(response);
                } else {
                    response.data = res.data.data;
                    response.total = res.data.total;
                    callback(response);
                }
            })
        }
    }

    /**
     * save folder in db
     * @param event get api request data
     * @param callback
     */
    addFolder(event, callback) {
        var response = {
            error: false,
            message: "",
            data: null
        };
        let validationSchema = {};
        let result = v.validateform(Object.assign(event.config, event.payload), validationSchema);

        if (result.error) {
            response.error = true;
            response.message = "Invalid Request Parameters or configuration missing";
            callback(response)
        } else {
            let params = event.payload;
            mysqlUtility.saveFolder(event.config, params, (res) => {
                if (res.error) {
                    response.error = true;
                    response.message = res.msg;
                    callback(response)
                } else {
                    response.data = res.data;
                    callback(response);
                }
            });
        }
    }

    /**
     * update folder in db
     * @param event get api request data
     * @param callback
     */
    updateFolder(event, callback) {
        var response = {
            error: false,
            message: "",
            data: null
        };
        let validationSchema = {};
        let result = v.validateform(Object.assign(event.config, event.payload), validationSchema);
        if (result.error) {
            response.error = true;
            response.message = "Invalid Request Parameters or configuration missing";
            callback(response)
        } else {
            const connection = mysqlUtility.dbConnection(event.config);
            mysqlUtility.updateFolder(connection, event, (res) => {
                connection.end();
                if (res.error) {
                    response.error = true;
                    response.message = res.msg;
                    callback(response);
                } else {
                    response.data = res.data;
                    callback(response);
                }
            });
        }
    }

    /**
     * delete folder
     * @param event get api request data
     * @param callback
     */
    deleteFolder(event, callback) {
        var response = {
            error: false,
            message: "",
            data: null
        };
        let validationSchema = {};
        let result = v.validateform(Object.assign(event.config, event.payload), validationSchema);
        if (result.error) {
            response.error = true;
            response.message = "Invalid Request Parameters or configuration missing";
            callback(response)
        } else {
            let params = event.queryParam;
            mysqlUtility.deleteFolder(event.config, params, (res) => {
                if (res.error) {
                    response.error = true;
                    response.message = res.msg;
                    callback(response);
                } else {
                    response.data = res.data;
                    callback(response);
                }
            });
        }
    }
}