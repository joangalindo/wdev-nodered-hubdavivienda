var S3 = require('aws-sdk/clients/s3');

var s3Utility = {};
const s3 = new S3({ accessKeyId: "AKIAR56B32NBTA52QJ5R", secretAccessKey: "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" });
const bucketName = "demo.wsuite.com";

/**
 * get file extention
 * @param fileName filename for get extention
 */
s3Utility.getExtention = (fileName) => {
    const fileExt = fileName.split('.').pop();
    return fileExt.toLowerCase();
}

s3Utility.getFilenameWithoutExt = function (fileName) {
    return fileName.split('.').slice(0, -1).join('.');
}

s3Utility.getCharacterBetweenTwoCharacters = function (fullstring, prefix, sufix) {
    return fullstring.substring(
        fullstring.lastIndexOf(prefix) + 1,
        fullstring.lastIndexOf(sufix)
    );
}

/**
 * get file count for same file name
 * @param fileName with search bucket same prefix filename
 * @param path which folder in search
 */
s3Utility.s3GetLastFileNumberForSameName = function (fileName, path, callback) {
    const params = {
        Bucket: bucketName,
        Delimiter: '/',
        Prefix: this.getFilenameWithoutExt(fileName)
    };

    s3.listObjects(params).promise().then((res) => {
        if (res.Contents.length === 1) {
            callback(1);
        } else if (res.Contents.length > 1) {
            const fileList = res.Contents.sort((a, b) => parseInt(this.getCharacterBetweenTwoCharacters(a.Key, '(', ')')) - parseInt(this.getCharacterBetweenTwoCharacters(b.Key, '(', ')')) ? 1 : -1);
            const lastFileNumber = this.getCharacterBetweenTwoCharacters(fileList[0].Key, '(', ')');
            callback((parseInt(lastFileNumber) + 1));
        } else {
            callback(0);
        }
    }).catch((e) => {
        console.error(e);
        callback(0);
    });
};

s3Utility.s3GetFile = function (fileName, callback) {
    const getObjParams = {
        Bucket: bucketName,
        Key: fileName
    };
    s3.getObject(getObjParams).promise().then((res) => {
        callback(null, res.Body);
    });
};

s3Utility.s3FileUploads = function (fileName, fileData, callback) {
    this.s3GetLastFileNumberForSameName(fileName, "", (res) => {
        if (res > 0) { // Upload file with rename
            fileName = this.getFilenameWithoutExt(fileName) + ' (' + res + ').' + this.getExtention(fileName);
        }
        const params = {
            Bucket: bucketName,
            Key: fileName,
            Body: fileData
        };
        s3.upload(params, (s3Err, data) => {
            if (s3Err) { console.error(s3Err); callback(s3Err, null); }
            callback(null, { fileName, fileLocation: data.Location });
        }).on('httpUploadProgress', (p) => {
            // callback(null, Math.round((p.loaded * 100) / p.total));
        });
    });
};

module.exports = s3Utility;