var moment = require('moment');
var mysql = require('mysql');
var mysqlUtility = {};
var response = {
    error: false,
    msg: "",
    data: null
};

mysqlUtility.dbConnection = (config) => {
    return mysql.createConnection({
        host: config.dbhost,
        user: config.dbusername,
        password: config.dbpassword,
        database: config.dbname
    });
};

/**
 * Get folder id
 */
mysqlUtility.getFolderId = function (config, folderName, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select * from CARPETAS where NOMBRE = '${folderName}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
        } else {
            response.data = (data[0] && data[0].id) ? data[0].id : null;
        }
        callback(response);
    });
};

/**
 * Get access type id
 */
mysqlUtility.getAccessTypeId = function (config, accessName, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select *from access where name='${accessName}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
        } else {
            response.data = (data[0] && data[0].id) ? data[0].id : null;
        }
        callback(response);
    });
};

/**
 * Get file extention id if not found with db then insert first then return inserted id
 */
mysqlUtility.getExtentionIdWithInsert = function (config, params, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select *from formats where extension='${params.name}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
            callback(response);
        } else {
            const formatId = (data[0] && data[0].id) ? data[0].id : null;
            if (formatId === null) {
                mysqlUtility.getContentTypesIdWithInsert(config, params.content, (res) => {
                    if(res.error){
                        callback(res);
                    }else{
                        let formatInsertQuery = `INSERT INTO formats(name, extension, idcontent_type) VALUES (?,?,?)`;
                        let formatInsertQueryParams = [params.name, params.extention, res.data];
                        connection.query(formatInsertQuery, formatInsertQueryParams, (err, results, fields) => {
                            if (err) {
                                response.error = true;
                                response.msg = err.sqlMessage;
                                callback(response);
                            } else {
                                response.data = results.insertId;
                                callback(response);
                            }
                        });
                    }
                });
            } else {
                response.data = formatId;
                callback(response);
            }
        }
    });
};

/**
 * Get content type id if not found with db then insert first then return inserted id
 */
mysqlUtility.getContentTypesIdWithInsert = function (config, name, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select *from content_types where name='${name}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
            callback(response);
        } else {
            const contentTypeId = (data[0] && data[0].id) ? data[0].id : null;
            if (contentTypeId === null) {
                let contentTypeQuery = `INSERT INTO content_types(name) VALUES (?)`;
                let contentTypeQueryParams = [name];
                connection.query(contentTypeQuery, contentTypeQueryParams, (err, results, fields) => {
                    if (err) {
                        response.error = true;
                        response.msg = err.sqlMessage;
                        callback(response);
                    } else {
                        response.data = results.insertId;
                        callback(response);
                    }
                });
            } else {
                response.data = contentTypeId;
                callback(response);
            }
        }
    });
};

/**
 * Get folder id if not found with db then insert first then return inserted id
 */
mysqlUtility.getFolderIdWithInsert = function (config, name, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select *from folders where name='${name}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
            callback(response);
        } else {
            const folderId = (data[0] && data[0].id) ? data[0].id : null;
            if (folderId === null) {
                let foldersQuery = `INSERT INTO folders (name) VALUES (?)`;
                let foldersQueryParams = [name];
                connection.query(foldersQuery, foldersQueryParams, (err, results, fields) => {
                    if (err) {
                        response.error = true;
                        response.msg = err.sqlMessage;
                        callback(response);
                    } else {
                        response.data = results.insertId;
                        callback(response);
                    }
                });
            } else {
                response.data = folderId;
                callback(response);
            }
        }
    });
};

/**
 * Get access type id if not found with db then insert first then return inserted id
 */
mysqlUtility.getAccessTypeIdWithInsert = function (config, name, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `select *from access where name='${name}'`;
    connection.query(query, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
            callback(response);
        } else {
            const contentTypeId = (data[0] && data[0].id) ? data[0].id : null;
            if (contentTypeId === null) {
                let accessTypeQuery = `INSERT INTO access (name) VALUES (?)`;
                let accessTypeQueryParams = [name];
                connection.query(accessTypeQuery, accessTypeQueryParams, (err, results, fields) => {
                    if (err) {
                        response.error = true;
                        response.msg = err.sqlMessage;
                        callback(response);
                    } else {
                        response.data = results.insertId;
                        callback(response);
                    }
                });
            } else {
                response.data = contentTypeId;
                callback(response);
            }
        }
    });
};

/**
 * save folder
 */
mysqlUtility.saveFolder = function (config, params, callback) {
    const connection = mysqlUtility.dbConnection(config);
    let query = `INSERT INTO CARPETAS (ID_CARPETA, NOMBRE, ID_USUARIO) VALUES (?,?,?)`;
    let queryParams = [params.IdFolder, params.NameFolder, params.IdUser];
    connection.query(query, queryParams, function (err, results) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
        } else {
            response.data = results.insertId;
        }
        callback(response);
    });
};

/**
 * Get folder list or details
 */
mysqlUtility.getFolder = (config, params, callback) => {
    const connection = mysqlUtility.dbConnection(config);
    const responseData = {};
    let queryFields = "F.ID_CARPETA, F.NOMBRE, F.ID_USUARIO";
    let queryCountFields = "count(F.ID_CARPETA) AS total_records";
    let query = `select __FIELDS__
                from CARPETAS AS F`;

    let countQuery = query.replace("__FIELDS__", queryCountFields);
    connection.query(countQuery, function (err, data) {
        if (err) {
            response.error = true;
            response.msg = err.sqlMessage;
            callback(response);
        } else {
            responseData.total = (data[0]) ? data[0].total_records : 0;
            if (params.id) {
                query += ` WHERE F.ID_CARPETA = ${params.id}`;
            } else {
                query += ` ORDER BY F.ID_CARPETA DESC`;
                if (params.limit) {
                    limit = params.limit;
                    let offset = 0;
                    if (params.page) {
                        offset = ((Number(params.page) - 1) * limit);
                    }
                    query += ` LIMIT ${limit} OFFSET ${offset}`;
                }
            }

            let dataQuery = query.replace("__FIELDS__", queryFields);
            connection.query(dataQuery, function (err, data) {
                if (err) {
                    response.error = true;
                    response.msg = err.sqlMessage;
                    callback(response);
                } else {
                    responseData.data = (params.id && data[0]) ? data[0] : data;
                    response.data = responseData;
                    callback(response);
                }
            });
        }
    });
};

/**
 * Delete Folder with each properties
 */
mysqlUtility.deleteFolder = function (config, params, callback) {
    const connection = mysqlUtility.dbConnection(config);
    this.deleteFileProperty(config, params, (res) => {
        if (res.error) {
            response.error = true;
            response.message = res.msg;
            callback(response);
        } else {
            let query = `DELETE FROM CARPETAS WHERE ID_CARPETA = ?`;
            connection.query(query, params.id, function (err, data) {
                if (err) {
                    response.error = true;
                    response.msg = err.sqlMessage;
                } else {
                    response.data = data;
                }
                callback(response);
            });
            callback(response);
        }
    });
};

/**
 * Update folder
 */
mysqlUtility.updateFolder = function (connection, event, callback) {
    this.getFolder(event.config, { id: event.queryParam.id }, (res) => {
        let query = `UPDATE CARPETAS SET NOMBRE = ?, ID_USUARIO = ? WHERE ID_CARPETA = ?`;
        let queryParams = [event.payload.NameFolder, event.payload.IdUser, event.queryParam.id];
        connection.query(query, queryParams, function (err, data) {
            if (err) {
                response.error = true;
                response.msg = err.sqlMessage;
            } else {
                response.data = null;
            }
            callback(response);
        });
    })
};

module.exports = mysqlUtility;