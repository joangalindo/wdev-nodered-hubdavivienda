var app = require('../core/app')
const wlog = require('wsuite-logger');
wlog.init();

exports.handler = async (e) => {
    console.log('Request e {0}'.format(JSON.stringify(e, null, 4)))
    const execution = e.Execution.Name;
    const configArr = e.Config;
    const config = configArr[execution];
    const headers = e.headers;
    const payload = (headers.httpmethod === 'get') ? headers.query : e.Payload;
    const queryParam = (headers.query) ? headers.query : {};

    // console.log('Request e payload {0}'.format(JSON.stringify(payload, null, 4)))
    // console.log('Request e config {0}'.format(JSON.stringify(config, null, 4)))
    // console.log('Request e headers {0}'.format(JSON.stringify(headers, null, 4)))
    console.log("e", e);

    var event = {
        payload: payload,
        config: config,
        queryParam: queryParam
    };

    console.log("event", event);
    var core = new app();
    return new Promise((resolve, reject) => {
        switch (config.name) {
            case 'Read-folder':
                core.listFolder(event, (res) => {
                    if (res.error) { reject(res.message) } else { resolve(res.data) }
                });
                break;
            case 'Create-folder':
                core.addFolder(event, (res) => {
                    if (res.error) { reject(res.message) } else { resolve(res.data) }
                });
                break;
            case 'Update-folder':
                core.updateFolder(event, (res) => {
                    if (res.error) { reject(res.message) } else { resolve(res.data) }
                });
                break;
            case 'Delete-folder':
                core.deleteFolder(event, (res) => {
                    if (res.error) { reject(res.message) } else { resolve(res.data) }
                });
                break;
            default:
                reject(`Not found ${config.name} node`);
                break;
        }
    });
};